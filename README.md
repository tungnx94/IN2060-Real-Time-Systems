## [IN2060] Real-Time Systems

This repository contains all the resources given to the students of Technische Universität München during the academic year 2018-2019.
Here you can find the slides and exercises downloaded from TUM's moodle and the solutions of said exercises.

The following file index has the original format and so the names do not match, because files have been renamed to have a more human-readable format.
Anyway, the links will guide you to the correct renamed files.

## Echtzeitsysteme (IN2060)

**General:**

General Information: If you want to do an exam bonus project, please read the document below and register your project on time.

- [Bonus Project][bonus-project]

------

### 18th October 2018 - 24th October 2018

- [Lecture 1: Introduction][slide-1]

------

### 25th October 2018 - 31st October 2018

- [Lecture 2: Clocks][slide-2]

------

### 1st November 2018 - 7th November 2018

- [Lecture 3: Hardware][slide-3]

------

### 8th November 2018 - 14th November 2018

- [Exercise 1 - PTP][exercise-1]

------

### 15th November 2018 - 21st November 2018

- [Lecture 4: Memory Impact on Timing][slide-4-1]
- [Exercise - C Tutorial][exercise-2]

------

### 22nd November 2018 - 28th November 2018

- [Lecture 5: RTOS][slide-5]
- [Lecture 4: Cache Example][slide-4-2]
- [C_Tutorial_II][exercise-3]

------

### 29th November 2018 - 5th November 2018

- [Lecture 6: Modelling][slide-6]
- [Cache - Direct Mapping][exercise-4]

------

### 6th December 2018

LECTURE AND EXERCISES CANCELLED, DIES ACADEMICUS

------

### 13th December 2018 - 19th December 2018

- [Lecture 7: Scheduling][slide-7-1]
- [Periodic Scheduling][slide-7-2]
- [Exercise][exercise-5]

------

### 20th December 2018 - 26th December 2018

LECTURE AND EXERCISES CANCELLED, enjoy your break over Christmas and New year's!

------

### 27th December 2018 - 2nd January 2019

------

### 3rd January 2019 - 9th January 2019

------

### 10th January 2019 - 16th January 2019

- [Lecture 8: Concurrency and Resources][slide-8]
- [Exercise][exercise-6]

------

### 17th January 2019 - 23rd January 2019

- [Lecture 9: Concurrency and Resources][slide-9-1]
- [CAN Bus Introduction (TI)][slide-9-2]
- [CAN Bus Physical Layer (TI)][slide-9-3]
- [Exercise][exercise-7]

------

### 24th January 2019 - 30th January 2019

- [Lecture 10: Dependability][slide-10-1]
- [CAN Special Tutorial][slide-10-2]

------

### 31st January 2019 - 6th February 2019

- [Exam Preparation][exam]

------

### 7th February 2019 - 13th February 2019


[bonus-project]: Slides/Bonus%20project.pdf "Bonus project"

[slide-1]:    Slides/1%20-%20Introduction.pdf                                                     "Slide 1 — Introduction"
[slide-2]:    Slides/2%20-%20Clocks.pdf                                                           "Slide 2 — Clocks"
[slide-3]:    Slides/3%20-%20Embedded%20hardware.pdf                                              "Slide 3 — Embedded hardware"
[slide-4-1]:  Slides/4.1%20-%20Memory%20hierarchy.pdf                                             "Slide 4 — Memory hierarchy (1)"
[slide-4-2]:  Slides/4.2%20-%20Cache%20example.pdf                                                "Slide 4 — Cache example (2)"
[slide-5]:    Slides/5%20-%20Real-Time%20Operating%20Systems.pdf                                  "Slide 5 — Real-Time Operating Systems"
[slide-6]:    Slides/6%20-%20Model-driven%20design.pdf                                            "Slide 6 — Model-driven design"
[slide-7-1]:  Slides/7.1%20-%20Scheduling.pdf                                                     "Slide 7 — Scheduling (1)"
[slide-7-2]:  Slides/7.2%20-%20Periodic%20scheduling.pdf                                          "Slide 7 — Periodic scheduling (2)"
[slide-8]:    Slides/8%20-%20Concurrency%20and%20processes.pdf                                    "Slide 8 — Concurrency and processes"
[slide-9-1]:  Slides/9.1%20-%20Real-Time%20communication.pdf                                      "Slide 9 — Real-Time communiaction (1)"
[slide-9-2]:  Slides/9.2%20-%20Introduction%20to%20the%20Controller%20Area%20Network.pdf          "Slide 9 — Introduction to the Controller Area Network (2)"
[slide-9-3]:  Slides/9.3%20-%20Controller%20Area%20Network%20physical%20layer%20requirements.pdf  "Slide 9 — Controller Area Network physical layer requirements (3)"
[slide-10-1]: Slides/10.1%20-%20Dependability.pdf                                                 "Slide 10 — Dependability (1)"
[slide-10-2]: Slides/10.2%20-%20Distributed%20control%20with%20the%20Control%20Area%20Network.pdf "Slide 10 — Distributed control with the Control Area Network (2)"

[exercise-1]: Exercises/1%20-%20Precision%20Time%20Protocol.pdf                                            "Exercise 1 — Precision Time Protocol"
[exercise-2]: Exercises/2%20-%20Introduction%20to%20C%20-%20Variables,%20types%20and%20control%20flow.pdf  "Exercise 2 — Introduction to C (Variables, types and control flow)"
[exercise-3]: Exercises/3%20-%20Introduction%20to%20C%20-%20Complex%20types,%20pointers%20and%20memory.pdf "Exercise 3 — Introduction to C (Complex types, pointers and memory)"
[exercise-4]: Exercises/4%20-%20Cache.pdf                                                                  "Exercise 4 — Cache"
[exercise-5]: Exercises/5%20-%20Periodic%20scheduling.pdf                                                  "Exercise 5 — Periodic scheduling"
[exercise-6]: Exercises/6%20-%20Schedulability%20testing.pdf                                               "Exercise 6 — Schedulability testing"
[exercise-7]: Exercises/7%20-%20Communication.pdf                                                          "Exercise 7 — Communication"

[exam]: Exam%20preparation.pdf "Exam preparation"
